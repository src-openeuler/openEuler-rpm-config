%global vendor %{?_vendor:%{_vendor}}%{!?_vendor:openEuler}
%global rpmvdir /usr/lib/rpm/%{vendor}

Name:		%{vendor}-rpm-config
Version:	31
Release:	20
License:	GPL+ AND MIT
Summary:	specific rpm configuration files
URL:		https://gitee.com/src-openeuler/openEuler-rpm-config
Buildarch:	noarch

# Core rpm settings
Source0: macros
Source1: rpmrc

# gcc specs files for hardened builds
Source10: generic-hardened-cc1
Source11: generic-hardened-ld
Source12: generic-pie-cc1 
Source13: generic-pie-ld

# clang config spec files
Source51: generic-hardened-clang.cfg

# The macros defined by these files are for things that need to be defined
# at srpm creation time when it is not feasible to require the base packages
# that would otherwise be providing the macros.
Source100: macros.perl
Source101: macros.forge
Source102: macros.go
Source103: macros.python
Source104: macros.kmp
Source105: macros.shell-completions
Source106: macros.package-notes-srpm

# Dependency generator scripts
Source200: find-requires.ksyms
Source201: infopages.attr
Source202: manpages.attr
Source203: nbdkit.attr
Source204: find-provides.nbdkit
# Source 205-208 from fedora, licenced under MIT
Source205: kabi.attr
Source206: kabi.sh
Source207: kmod.attr
Source208: libsymlink.attr

# Produce "bundled(golang(MODULE_NAME)) = VERSION" provides if there are
# vendored sources inside source directory.
Source209: golangvendorbundled.attr
# Source 210 from fedora, licened under GPL-2.0-or-later
# https://src.fedoraproject.org/rpms/golang/blob/rawhide/f/bundled-deps.sh
# modified to be used as dependency generator
Source210: find-provides.golang-vendor-bundled

# Misc helper scripts
Source300: kmodtool
Source301: find-requires
Source303: fix-libtool-from-moving-options-after-libs
Source304: package-notes.in

# Snapshots from http://git.savannah.gnu.org/gitweb/?p=config.git
Source500: config.guess
Source501: config.sub
Patch0001: 0001-support-sw_64-arch.patch

# BRPs
Source700: brp-chrpath
Source701: brp-digest-list
Source702: brp-ebs-sign
Source703: brp-ldconfig
Source704: brp-remove-info-dir
Source705: brp-check-elf-files
Source706: brp-strip-lto
Source707: brp-clean-perl-files
Source708: brp-llvm-compile-lto-elf

# Convenience lua functions
Source800: common.lua

Provides: python-rpm-macros = %{?epoch:%{epoch}:}%{version}-%{release}
Provides: python2-rpm-macros = %{?epoch:%{epoch}:}%{version}-%{release}
Provides: python3-rpm-macros = %{?epoch:%{epoch}:}%{version}-%{release}
Provides: python-srpm-macros = %{?epoch:%{epoch}:}%{version}-%{release}
Provides: fpc-srpm-macros = 1.1-6
Provides: ghc-srpm-macros = 1.4.2-8
Provides: gnat-srpm-macros = 4-6
Provides: nim-srpm-macros = 1-3
Provides: ocaml-srpm-macros = 5-4
Provides: openblas-srpm-macros = 2-4
Provides: perl-srpm-macros = 1-28
Provides: rust-srpm-macros = 10-1
Provides: go-srpm-macros = 2-18
Provides: perl-macros = 4:5.32.0-1
Provides: nbdkit-srpm-macros = %{?epoch:%{epoch}:}%{version}-%{release}
Provides: package-notes-srpm-macros = %{?epoch:%{epoch}:}%{version}-%{release}
Obsoletes: perl-macros < 4:5.32.0-1
Obsoletes: python-rpm-macros < %{?epoch:%{epoch}:}%{version}-%{release}
Obsoletes: python2-rpm-macros < %{?epoch:%{epoch}:}%{version}-%{release}
Obsoletes: python3-rpm-macros < %{?epoch:%{epoch}:}%{version}-%{release}
Obsoletes: python-srpm-macros < %{?epoch:%{epoch}:}%{version}-%{release}
Obsoletes: fpc-srpm-macros < 1.1-6
Obsoletes: ghc-srpm-macros < 1.4.2-8
Obsoletes: gnat-srpm-macros < 4-6
Obsoletes: nim-srpm-macros < 1-3
Obsoletes: ocaml-srpm-macros < 5-4
Obsoletes: openblas-srpm-macros < 2-4
Obsoletes: perl-srpm-macros < 1-28
Obsoletes: rust-srpm-macros < 10-1
Obsoletes: go-srpm-macros < 2-18

%if "%{vendor}" != "openEuler"
Provides: openEuler-rpm-config = %{?epoch:%{epoch}:}%{version}-%{release}
Obsoletes: openEuler-rpm-config < %{?epoch:%{epoch}:}%{version}-%{release}
%endif

Requires: efi-srpm-macros
Requires: qt5-srpm-macros

BuildRequires: %{vendor}-release

Requires: rpm >= 4.17.0
Requires: rpm-build >= 4.17.0
Requires: zip
Requires: curl
#Requires: (annobin if gcc)

# for brp-mangle-shebangs
Requires: %{_bindir}/find
Requires: %{_bindir}/file
Requires: %{_bindir}/grep
Requires: %{_bindir}/sed
Requires: %{_bindir}/xargs
Requires: %{_bindir}/chrpath
Requires: %{_bindir}/eu-elfclassify

# for brp-llvm-compile-lto-elf
Requires: (llvm if clang)
Requires: (gawk if clang)

Requires: coreutils

# -fstack-clash-protection and -fcf-protection require GCC 8.
Conflicts: gcc < 7

Provides: system-rpm-config = %{version}-%{release}

%description
specific rpm configuration files for %{vendor}.

%package -n kernel-rpm-macros
Summary: Macros and scripts for building kernel module packages

%description -n kernel-rpm-macros
Macros and scripts for building kernel module packages.

%prep
%setup -Tc -n %{name}-%{version}
cp %{_sourcedir}/* .
rm -f generic-rpm-config.yaml 
%autopatch -p1

%install
mkdir -p %{buildroot}%{rpmvdir}
install -p -m 644 -t %{buildroot}%{rpmvdir} macros rpmrc
install -p -m 755 -t %{buildroot}%{rpmvdir} config.*
install -p -m 755 -t %{buildroot}%{_rpmconfigdir} brp-*
install -p -m 644 -t %{buildroot}%{_rpmconfigdir} generic-*
install -p -m 755 -t %{buildroot}%{_rpmconfigdir} fix-libtool-from-moving-options-after-libs
install -p -m 755 -t %{buildroot}%{_rpmconfigdir} kabi.sh
install -p -m 755 -t %{buildroot}%{rpmvdir} kmodtool
install -p -m 755 -t %{buildroot}%{rpmvdir} find-requires*
install -p -m 755 -t %{buildroot}%{rpmvdir} find-provides*
install -p -m 755 package-notes.in %{buildroot}%{rpmvdir}/%{vendor}-package-notes

mkdir -p %{buildroot}%{_rpmconfigdir}/macros.d
install -p -m 644 -t %{buildroot}%{_rpmconfigdir}/macros.d/ macros.*

mkdir -p %{buildroot}%{_fileattrsdir}
install -p -m 644 -t %{buildroot}%{_fileattrsdir} *.attr

mkdir -p %{buildroot}%{_rpmluadir}/%{vendor}/{rpm,srpm}
install -p -m 644 -t %{buildroot}%{_rpmluadir}/%{vendor} common.lua

# Adaptive according to vendor
sed -i "s/@VENDOR@/%{vendor}/g" `grep "@VENDOR@" -rl %{buildroot}%{_rpmconfigdir}`
sed -i "s|@OSCPE@|$(cat /etc/system-release-cpe)|" %{buildroot}%{rpmvdir}/%{vendor}-package-notes

%files
%dir %{rpmvdir}
%{rpmvdir}/macros
%{rpmvdir}/rpmrc
%{_rpmconfigdir}/brp-*
%{rpmvdir}/config.*
%{rpmvdir}/find-provides.nbdkit
%{rpmvdir}/find-provides.golang-vendor-bundled
%{rpmvdir}/%{vendor}-package-notes
%{_rpmconfigdir}/generic-*
%{_rpmconfigdir}/fix-libtool-from-moving-options-after-libs
%{_fileattrsdir}/*.attr
%{_rpmconfigdir}/kabi.sh
%{_rpmconfigdir}/macros.d/
%{_rpmluadir}/%{vendor}/*.lua
%exclude %{_rpmconfigdir}/macros.d/macros.kmp

%files -n kernel-rpm-macros
%{rpmvdir}/kmodtool
%{_rpmconfigdir}/macros.d/macros.kmp
%{rpmvdir}/find-requires
%{rpmvdir}/find-requires.ksyms

%changelog
* Sat Mar 08 2025 Funda Wang <fundawang@yeah.net> - 31-20
- Add package information on ELF objects
- buildrequires vendor-release for /etc/system-release-cpe file
- unify vendor macro usage

* Tue Feb 25 2025 Funda Wang <fundawang@yeah.net> - 31-19
- fix golang-vendor-provides script for kubernetes
- move kabi and kmod attr into system-rpm-config so that it could be used when building kernel

* Thu Feb 20 2025 Funda Wang <fundawang@yeah.net> - 31-18
- Produce "bundled(golang(MODULE_NAME)) = VERSION" provides if there are
  vendored sources inside source directory

* Tue Feb 18 2025 Jun He <jun.he@arm.com> - 31-17
- Add compile flag (-mbranch-protection=standard) to enable PAC/BTI features on aarch64 as default

* Mon Feb 03 2025 Funda Wang <fundawang@yeah.net> - 31-16
- Make libfoo.so symlinks require the soname-provide of the target library
- revert previous change regarding perl_default_filter, it causes regressions
  for some packages

* Fri Jan 31 2025 Funda Wang <fundawang@yeah.net> - 31-15
- add kabi, kmod dependency generators from fedora

* Tue Jan 28 2025 Funda Wang <fundawang@yeah.net> - 31-14
- whitelist META.json/yml for scanning perl dependencies

* Fri Jan 17 2025 Funda Wang <fundawang@yeah.net> - 31-13
- split out fortify level definition for supporting build with FORTIFY_SOURCE=3 in the future

* Wed Jan 15 2025 luhuaxin <luhuaxin1@huawei.com> - 31-12
- ima: keep the process of OBS signing same as previous version

* Sun Jan 12 2025 Funda Wang <fundawang@yeah.net> - 31-11
- support sw_64 arch

* Mon Jan 06 2025 Funda Wang <fundawang@yeah.net> - 31-10
- add shell completions dir declaration

* Wed Dec 11 2024 Funda Wang <fundawang@yeah.net> - 31-9
- update py_install_wheel declaration

* Sat Nov 16 2024 Funda Wang <fundawang@yeah.net> - 31-8
- compile LLVM IR bitcode in object files or static libraries
  into ELF object code for clang < 18

* Sat Oct 26 2024 Funda Wang <fundawang@yeah.net> - 31-7
- remove useless perl .packlist, empty .bs files and
  wrongly created perllocal.pod from buildroot

* Sat Oct 19 2024 Funda Wang <fundawang@yeah.net> - 31-6
- add extension flags for python modules

* Thu Sep 19 2024 Funda Wang <fundawang@yeah.net> - 31-5
- Drop symlinks of .la and .a also
- move hardcode requires on man and info to file attrs
- add helper script for mageia to fix libtool linking order
- disable info dir generatiion by setting AM_UPDATE_INFO_DIR=no
- add helper macro for extensions of man pages and info pages
- add helper scripts for nbdkit

* Fri Sep 06 2024 Funda Wang <fudnawang@yeah.net> - 31-4
- Enable LTO by default

* Thu Aug 29 2024 Funda Wang <fudnawang@yeah.net> - 31-3
- Output elf check result for maintainer to analyse

* Thu Aug 29 2024 Funda Wang <fudnawang@yeah.net> - 31-2
- remove /usr/share/info/dir in buildroot automatically

* Sun Aug 25 2024 Funda Wang <fudnawang@yeah.net> - 31-1
- merge two messy repositories
- update config.sub and config.guess, without support on loongarchx32,
  as it does not exist in real world
- add chrpath and coreutils runtime dependecies
- bump rpm version requirement for brp-remove-la-files
- package becomes noarch now

* Tue Aug 6 2024 liyunfei <liyunfei33@huawei.com> - 30-58
- Fix for "%undefine _auto_set_build_XX" usage

* Wed Jul 10 2024 xujing <xujing125@huawei.com> - 30-57
- enable --as-needed by default

* Mon Apr 29 2024 xujing <xujing125@huawei.com> - 30-56
- don't delete the commented code in macros

* Sun Apr 28 2024 laokz <zhangkai@iscas.ac.cn> - 30-55
- add riscv64 to some arches macro

* Sun Apr 7 2024 zhangguangzhi <zhangguangzhi3@huawei.com> - 30-54
- ima digest list ebs sign ret 2 when errmsg is SIGN_PERMISSION_DENIED

* Fri Mar 29 2024 zhangguangzhi <zhangguangzhi3@huawei.com> - 30-53
- ima digest list ebs sign use file path and check errmsg

* Fri Mar 22 2024 zhangguangzhi <zhangguangzhi3@huawei.com> - 30-52
- ima digest list ebs sign support modsig

* Fri Mar 15 2024 yueyuankun <yueyuankun@kylinos.cn> - 30-51
- Add optflags for loongarch64 and sw_64

* Tue Mar 12 2024 liyunfei <liyunfei33@huawei.com> - 30-50
- Add clang toolchain support

* Tue Mar 5 2024 hongjinghao <hongjinghao@huawei.com> - 30-49
- Delete the commented code

* Tue Feb 20 2024 peng.zou <peng.zou@shingroup.cn> - 30-48
- add powerpc64le to generic_arches

* Mon Dec 11 2023 jiahua.yu <jiahua.yu@shingroup.cn> - 30-47
- Init support for arch ppc64le

* Wed Nov 22 2023 xujing <xujing125@huawei.com> - 30-46
- add the scanning path of the rpath
  fix the ELF file cannot be found due to escape of '\'
  excute brp_chrpath before arch_install_post

* Fri Nov 03 2023 fuanan <fuanan3@h-partners.com> - 30-45
- check if the file is a symbolic link in brp-digest-list

* Thu Nov 2 2023 Yang Yanchao <yangyanchao6@huawei.com> - 30-44
- kmodtool: use tmp.$$ instand of tmp.txt

* Mon Aug 28 2023 liyunfei <liyunfei33@huawei.com> - 30-43
- Revert backport toolchain selecting and %%auto_set_build_flags patches

* Thu Aug 17 2023 liyunfei <liyunfei33@huawei.com> - 30-42
- Backport Call %%set_build_flags before %%build, %%check, and %%install stages

* Thu Aug 17 2023 liyunfei <liyunfei33@huawei.com> - 30-41
- Backport Add support for selecting a clang as a tool

* Mon Jul 24 2023 Dongxing Wang <dxwangk@isoftstone.com> - 30-40
- add pytest and tox macros

* Thu May 11 2023 caodongxia <caodongxia@h-partners.com> - 30-39
- improve pyproject_install macro

* Thu Apr 20 2023 caodongxia <caodongxia@h-partners.com> - 30-38
- support pyproject compilation

* Fri Mar 24 2023 laokz <zhangkai@iscas.ac.cn> - 30-37
- fix riscv64 default library directory of brp-ldconfig

* Fri Mar 17 2023 Xinliang Liu <xinliang.liu@linaro.org> - 30-36
- Fix kmod rpm install failed.

* Sat Jan 14 2023 luhuaxin <luhuaxin1@huawei.com> - 30-35
- support EBS sign

* Wed Dec 14 2022 huajingyun <huajingyun@loongson.cn> - 30-34
- fix config error for loongarch64

* Tue Dec 13 2022 Wenlong Zhang <zhangwenlong@loongson.cn> - 30-33
- add loongarch64 for golang_arches

* Wed Dec 7 2022 yangmingtai <yangmingtai@huawei.com> - 30-32
- fix latest_kernel macro

* Wed Dec 7 2022 Yang Yanchao <yangyanchao6@huawei.com> - 30-31
- backport kmp feature

* Wed Nov 30 2022 yangmingtai <yangmingtai@huawei.com> - 30-30
- support Adaptive according to vendor

* Mon Nov 21 2022 huajingyun <huajingyun@loongson.cn> - 30-29
- add loongarch64 support

* Wed Oct 12 2022 yangmingtai <yangmingtai@huawei.com> - 30-28
- macro.kmp support -p preamble

* Thu Sep  8 2022 yangmingtai <yangmingtai@huawei.com> - 30-27
- add find-requires and find-requires.ksyms

* Mon Jun 13 2022 yangmingtai <yangmingtai@huawei.com> - 30-26
- fix build failed, bare words are no longer supported

* Mon Dec 13 2021 Liu Zixian <liuzixian4@huawei.com> - 30-25
- fix python macros

* Fri Nov 26 2021 shixuantong <shixuantong@huawei.com> - 30-24
- update the definition of python3_version

* Wed Oct 13 2021 wangkerong <wangkerong@huawei.com> - 30-23
- add common lua scripts resolve "%%fontpkg" macro translation failure

* Sat Sep 4 2021 yangmingtai <yangmingtai@huawei.com> - 30-22
- add brp scripts to delete rpath

* Thu Apr 8 2021 Anakin Zhang <benjamin93@163.com> - 30-21
- exclude kernel source and EFI files in digest list building

* Mon Mar 29 2021 shenyangyang <shenyangyang4@huawei.com> - 30-20
- Patched missing patch that remove fexceptions

* Thu Mar 25 2021 shenyangyang <shenyangyang4@huawei.com> - 30-19
- Modify support for change vendor with better method

* Thu Mar 18 2021 shenyangyang <shenyangyang4@huawei.com> - 30-18
- Change the name of spec to openEuler-rpm-spec and fix few bugs

* Thu Mar 11 2021 shenyangyang <shenyangyang4@huawei.com> - 30-17
- Add for support for change vendor

* Tue Dec 1 2020 whoisxxx <zhangxuzhou4@huawei.com> - 30-16
- Add riscv64 in macros.go

* Wed Sep 30 2020 shenyangyang <shenyangyang4@huawei.com> - 30-15
- Change the source code to tar

* Fri Aug 21 2020 Wang Shuo <wangshuo_1994@foxmail.com> - 30-14
- fix error message for kmodtool

* Thu Aug 13 2020 shenyangyang <shenyangyang4@huawei.com> - 30-13
- Add provides of perl-macros

* Thu Aug 6 2020 tianwei <tianwei12@huawei.com> - 30-12
- delete strip-file-prefix

* Mon Aug 3 2020 Anakin Zhang <benjamin93@163.com> - 30-12
- add brp-digest-list

* Fri Jun 19 2020 zhangliuyan <zhangliuyan@huawei.com> - 30-11
- add kmodtool.py macros.kmp

* Wed May 6 2020 openEuler Buildteam <buildteam@openeuler.org> - 30-10
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: disable buildid link macro

* Tue Feb 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 30-9
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:modify python_provide macro from python2 to python3

* Sun Jan 19 2020 openEuler Buildteam <buildteam@openeuler.org> - 30-8
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:revise vendor in macro

* Sun Jan 19 2020 openEuler Buildteam <buildteam@openeuler.org> - 30-7
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:revise macro file

* Tue Jan 14 2020 openEuler Buildteam <buildteam@openeuler.org> - 30-6
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update macros file

* Tue Jan 14 2020 openEuler Buildteam <buildteam@openeuler.org> - 30-5
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add macros to macros.python

* Mon Jan 13 2020 openEuler Buildteam <buildteam@openeuler.org> - 30-4
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update macros.python

* Mon Jan 13 2020 openEuler Buildteam <buildteam@openeuler.org> - 30-3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:change type of files

* Mon Jan 13 2020 openEuler Buildteam <buildteam@openeuler.org> - 30-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add source10 to package

* Mon Jan 13 2020 openEuler Buildteam <buildteam@openeuler.org> - 30-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:rebuild

* Thu Dec 26 2019 openEuler Buildteam <buildteam@openeuler.org> - 29-20
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:delete unneeded provides

* Wed Nov 27 2019 fanghuiyu<fanghuiyu@huwei.com> - 29-19
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:change to generic-rpm-config

* Fri Nov 15 2019 jiangchuangang<jiangchuangang@huwei.com> - 29-18
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: remove fcf-protection for x86_64 from rpmrc

* Wed Oct 30 2019 hexiaowen <hexiaowen@huawei.com> - 29-17
- add custom macros

* Wed Sep 25 2019 hexiaowen <hexiaowen@huawei.com> - 29-16
- add rust-srpm-macros and go-srpm-macros

* Fri Sep 20 2019 hexiaowen <hexiaowen@huawei.com> - 29-15
- add version-release for python-rpm-macros

* Fri Sep 20 2019 hexiaowen <hexiaowen@huawei.com> - 29-14
- add python-rpm-macros fpc-srpm-macros ghc-srpm-macros gnat-srpm-macros
- nim-srpm-macros ocaml-srpm-macros openblas-srpm-macros perl-srpm-macros

* Thu Aug 29 2019 hexiaowen <hexiaowen@huawei.com> - 29-13
- fix typo

* Tue Aug 27 2019 hexiaowen <hexiaowen@huawei.com> - 29-2
- delete annobin

* Wed Jul 18 2018 openEuler Buildteam <buildteam@openeuler.org> - 29-1
- Package init
